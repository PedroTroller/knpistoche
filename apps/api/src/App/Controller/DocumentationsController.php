<?php

namespace App\Controller;

use App\Repository\Products;
use Domain\UseCase\GetDocumentationForAProduct;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DocumentationsController
{
    /**
     * @var GetDocumentationForAProduct
     */
    private $useCase;

    /**
     * @var Products
     */
    private $products;

    public function __construct(GetDocumentationForAProduct $useCase, Products $products)
    {
        $this->useCase  = $useCase;
        $this->products = $products;
    }

    public function documentationAction(Request $request, string $identifier, string $format): Response
    {
        $product = $this->products->getProductFromIdentifier($identifier);

        if (null === $product) {
            throw new NotFoundHttpException('Product not found');
        }

        return new Response(($this->useCase)($product, $format));
    }
}

<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Domain\UseCase\ListAllProducts;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Domain\Repository\Products;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductsController
{
    /**
     * @var ListAllProducts
     */
    private $useCase;

    /**
     * @var NormalizerInterface
     */
    private $normalizer;

    public function __construct(ListAllProducts $useCase, NormalizerInterface $normalizer)
    {
        $this->useCase = $useCase;
        $this->normalizer = $normalizer;
    }

    public function listAction(Request $request): Response
    {
        return new JsonResponse(
            $this->normalizer->normalize(($this->useCase)())
        );
    }
}

<?php

namespace App\Documentation;

use Michelf\Markdown;
use Domain;

class MarkdownTransformer implements \Domain\Documentation\MarkdownTransformer
{
    public function fromMarkdownToHtml(string $string): string
    {
        return Markdown::defaultTransform($string);
    }
}

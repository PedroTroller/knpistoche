<?php

namespace App\Repository;

use Domain;
use Domain\Exception\DocumentationNotFound;
use Domain\Model\Product;

class Documentations implements \Domain\Repository\Documentations
{
    /**
     * @var string
     */
    private $root;

    public function __construct(string $root)
    {
        $this->root = $root;
    }

    public function getForProduct(Product $product): string
    {
        $path = sprintf(
            '%s%s%s.md',
            rtrim($this->root, DIRECTORY_SEPARATOR),
            DIRECTORY_SEPARATOR,
            $product->getIdentifier()
        );

        if (false === file_exists($path)) {
            throw new DocumentationNotFound(sprintf('File "%s" not found.', $path));
        }

        return file_get_contents($path);
    }
}

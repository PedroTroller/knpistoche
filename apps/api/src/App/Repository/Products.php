<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Domain\Model\Product;

class Products extends EntityRepository implements \Domain\Repository\Products
{
    public function getProductFromIdentifier(string $identifier):? Product
    {
        $object = $this->findOneBy(['identifier' => $identifier]);

        if ($object instanceof Product) {
            return $object;
        }

        return null;
    }

    public function listAllProducts(): iterable
    {
        return $this->findAll();
    }
}

<?php

namespace spec\Domain\Model;

use Domain\Model\Product;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ProductSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(
            '92ebb126-5ada-45c3-ae37-20dbe066837b',
            'Le Zouzou',
            5500,
            'https://www.leslipfrancais.fr/le-zouzou-marine-short-bleu-marine-810.html',
            ['S', 'M', 'L', 'XL']
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Product::class);
    }

    function it_has_an_identifier()
    {
        $this->getIdentifier()->shouldReturn('92ebb126-5ada-45c3-ae37-20dbe066837b');
    }

    function it_has_a_name()
    {
        $this->getName()->shouldReturn('Le Zouzou');
    }

    function it_has_a_price()
    {
        $this->getPrice()->shouldReturn(5500);
    }

    function it_as_a_manufacturer_link()
    {
        $this->getManufacturerLink()->shouldReturn('https://www.leslipfrancais.fr/le-zouzou-marine-short-bleu-marine-810.html');
    }

    function it_has_sizes()
    {
        $this->getSizes()->shouldReturn(['S', 'M', 'L', 'XL']);
    }
}

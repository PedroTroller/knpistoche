<?php

namespace spec\Domain\UseCase;

use Domain\UseCase\GetDocumentationForAProduct;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Domain\Model\Product;
use Domain\Repository\Documentations;
use Domain\Documentation\MarkdownTransformer;

class GetDocumentationForAProductSpec extends ObjectBehavior
{
    function let(Documentations $documentations, MarkdownTransformer $markdownTransformer)
    {
        $this->beConstructedWith($documentations, $markdownTransformer);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(GetDocumentationForAProduct::class);
    }

    function it_returns_the_html_content_of_the_markdown_documentation_for_a_product_with_html_format(Product $product, $documentations, $markdownTransformer)
    {
        $documentations->getForProduct($product)->willReturn('markdown');
        $markdownTransformer->fromMarkdownToHtml('markdown')->willReturn('html');

        $this($product, 'html')->shouldReturn('html');
    }

    function it_returns_the_html_content_of_the_markdown_documentation_for_a_product_with_markdown_format(Product $product, $documentations, $markdownTransformer)
    {
        $documentations->getForProduct($product)->willReturn('markdown');
        $markdownTransformer->fromMarkdownToHtml(Argument::cetera())->shouldNotBeCalled();

        $this($product, 'markdown')->shouldReturn('markdown');
    }
}

<?php

namespace spec\Domain\UseCase;

use Domain\UseCase\ListAllProducts;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Domain\Repository\Products;
use Domain\Model\Product;

class ListAllProductsSpec extends ObjectBehavior
{
    function let(Products $products)
    {
        $this->beConstructedWith($products);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ListAllProducts::class);
    }

    function it_returns_the_list_of_all_products($products, Product $p1, Product $p2, Product $p3)
    {
        $products->listAllProducts()->willReturn([
            $p1,
            $p2,
            $p3,
        ]);

        $this()->shouldReturn([
            $p1,
            $p2,
            $p3,
        ]);
    }
}

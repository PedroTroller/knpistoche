<?php

namespace Domain\Documentation;

interface MarkdownTransformer
{
    public function fromMarkdownToHtml(string $string): string;
}

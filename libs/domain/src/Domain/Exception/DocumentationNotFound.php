<?php

namespace Domain\Exception;

use Exception;

class DocumentationNotFound extends Exception
{
}

<?php

namespace Domain\Exception;

use Exception;

class UnsupportedDocumentationFormat extends Exception
{
    public function __construct(string $format, array $supported, int $code = 0, Exception $previous = null)
    {
        parent::__construct(
            sprintf(
                'Documentation format %s not supported, only %s accepted.',
                $format,
                join(' and ', $supported)
            ),
            $code,
            $previous
        );
    }
}

<?php

namespace Domain\Model;

class Product
{
    /**
     * @var string
     */
    private $identifier;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $price;

    /**
     * @var string
     */
    private $manufacturerLink;

    /**
     * @var string[]
     */
    private $sizes;

    public function __construct(
        string $identifier,
        string $name,
        int $price,
        string $manufacturerLink,
        array $sizes
    ) {
        $this->identifier = $identifier;
        $this->name = $name;
        $this->price = $price;
        $this->manufacturerLink = $manufacturerLink;
        $this->sizes = $sizes;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getManufacturerLink(): string
    {
        return $this->manufacturerLink;
    }

    public function getSizes(): array
    {
        return $this->sizes;
    }
}

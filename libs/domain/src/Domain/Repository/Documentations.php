<?php

namespace Domain\Repository;

use Domain\Model\Product;

interface Documentations
{
    /**
     * @throw \Domain\Exception\DocumentationNotFound
     */
    public function getForProduct(Product $product): string;
}

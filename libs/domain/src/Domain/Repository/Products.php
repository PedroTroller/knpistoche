<?php

namespace Domain\Repository;

use Domain\Model\Product;

interface Products
{
    public function getProductFromIdentifier(string $identifier):? Product;

    public function listAllProducts(): iterable;
}

<?php

namespace Domain\UseCase;

use Domain\Repository\Documentations;
use Domain\Documentation\MarkdownTransformer;
use Domain\Model\Product;
use Domain\Exception\UnsupportedDocumentationFormat;

class GetDocumentationForAProduct
{
    const FORMAT_HTML = 'html';
    const FORMAT_MARKDOWN = 'markdown';

    /**
     * @var Documentations
     */
    private $documentations;

    /**
     * @var MarkdownTransformer
     */
    private $markdownTransformer;

    public function __construct(
        Documentations $documentations,
        MarkdownTransformer $markdownTransformer
    ) {
        $this->documentations = $documentations;
        $this->markdownTransformer = $markdownTransformer;
    }

    /**
     * @throw UnsupportedDocumentationFormat
     */
    public function __invoke(Product $product, string $format): string
    {
        if (false === in_array($format, [self::FORMAT_HTML, self::FORMAT_MARKDOWN])) {
            throw new UnsupportedDocumentationFormat($format, [self::FORMAT_HTML, self::FORMAT_MARKDOWN]);
        }

        $markdown = $this->documentations->getForProduct($product);

        if (self::FORMAT_MARKDOWN === $format) {
            return $markdown;
        }

        return $this->markdownTransformer->fromMarkdownToHtml($markdown);
    }
}

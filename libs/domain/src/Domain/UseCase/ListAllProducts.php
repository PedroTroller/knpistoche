<?php

namespace Domain\UseCase;

use Domain\Repository\Products;

class ListAllProducts
{
    /**
     * @var Products
     */
    private $products;

    public function __construct(Products $products)
    {
        $this->products = $products;
    }

    public function __invoke()
    {
        return $this->products->listAllProducts();
    }
}
